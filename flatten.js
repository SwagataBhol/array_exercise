
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    function flatten(items) {
        let flattenArray=[]
        for(index=0;index<items.length;index++)
        {
            if(Array.isArray(items[index]))

            {
                //console.log(flattenArray)
                flattenArray=flattenArray.concat(flatten(items[index]))
            }
            else
            {
                flattenArray.push(items[index])
            }
        }
        return flattenArray;
    }
    module.exports=flatten