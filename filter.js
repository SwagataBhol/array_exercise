
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    function filter(items, cb) {
        let array=[]
        for(index=0;index<items.length;index++)
        {
            let value=items[index]
            if(cb(value))
            {
                array.push(value)
            }
        }
        return array
    }
    module.exports=filter