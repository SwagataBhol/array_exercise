
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
    function reduce(items, cb, startingValue) {
        
        
        if (startingValue===undefined)
        {
            startingValue=items[0]
        }
        let accumulator=0
        for(let index=0;index<items.length;index++)
        {
            let value=items[index]
           accumulator= cb(startingValue,value)
           startingValue=accumulator;
        }
        return accumulator ;
    }
    module.exports=reduce