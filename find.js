
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
    function find(items, cb) {
        let result=undefined
        for(let index=0;index<items.length;index++)
        {
            
            let value=items[index]
            result=cb(value)
            if (result===true)
            {
                return value
            }
            
        }
        return result
    }
    module.exports=find